import numpy as np
import pandas as pd
import pyomo.environ as pyo
import pyomo.mpec as mpec
from pyomo.opt import SolverFactory

####  MODEL DEFINITION  ########################################################

m            = pyo.AbstractModel()
m.I          = pyo.Set()                     # Firms
m.J          = pyo.Set()                     # Technologies
m.Y          = pyo.Set()                     # Years
m.L          = pyo.Set()                     # Load levels
m.T          = pyo.Param(          m.Y, m.L) # [h]          Hours in a load level
m.D_0        = pyo.Param(          m.Y, m.L) # [MW]         Demand_0
m.alpha      = pyo.Param(          m.Y, m.L) # [MW/(€/MWh)] Slope of the demand
m.gamma      = pyo.Param(     m.J, m.Y,    ) # [€/MWh]      Unitary cost
m.X          = pyo.Param(m.I, m.J, m.Y,    ) # [MW]         Technology capacity of a firm in one year
m.grad_P     = pyo.Param(          m.Y, m.L) # [-]          Price gradient
m.q          = pyo.Var(  m.I, m.J, m.Y, m.L) # Energy quantity
m.l_positive = pyo.Var(  m.I, m.J, m.Y, m.L) # Residual energy quantity
m.l_X        = pyo.Var(  m.I, m.J, m.Y     ) # Residual capacity 

def m_p(m,y,l): # alias
    """
        price = (D_0 - ∑_ij(q))/α ∀ y,l
    """
    return (m.D_0[y,l] - sum(m.q[i,j,y,l] for i,j in m.I*m.J))/m.alpha[y,l]


def gradient(m,i,j,y,l):  # Gradient of Lagrangian
    """
        ∇ L(q) = ∑_j(Θqt) - pt + σt - μ + λ = 0
    """
    return ((m.grad_P[y,l]*sum(m.q[i,jj,y,l] for jj in m.J)*m.T[y,l] - m_p(m,y,l)*m.T[y,l] + m.gamma[j,y]*m.T[y,l] - m.l_positive[i,j,y,l] + m.l_X[i,j,y]) == 0)

def c_X(m,i,j,y): # Capacity constraint
    """
        ∑_l(q) ≤ X ∀ i,j,y
    """
    return sum(m.q[i,j,y,l] for l in m.L) <= m.X[i,j,y]

def c_l_X_positive(m,i,j,y): # Lambda of c_X constraint
    """
        λ ≥ 0 ∀ i,j,y
    """
    return m.l_X[i,j,y] >= 0

def mpec_comp_X(m,i,j,y): # Complemetarity between c_X and c_l_X_positive
    """
        ∑_l(q) ≤ X ⊥  λ ≥ 0
    """
    return mpec.complements(c_l_X_positive(m,i,j,y), c_X(m,i,j,y))

def c_q_positive(m,i,j,y,l): # Quantity positive constraint
    """
        q ≥ 0 ∀ i,j,y,l
    """
    return m.q[i,j,y,l] >= 0

def c_l_positive(m,i,j,y,l): # Mu of q positive constraint
    """
        μ ≥ 0 ∀ i,j,y,l
    """
    return m.l_positive[i,j,y,l] >= 0

def mpec_comp_positive(m,i,j,y,l): # Complementarity between c_q_positive and c_l_positive
    """
        q ≥ 0 ⊥  μ ≥ 0
    """
    return mpec.complements(c_l_positive(m,i,j,y,l), c_q_positive(m,i,j,y,l))


m.gradient           = pyo.Constraint(      m.I, m.J, m.Y, m.L, rule = gradient)
m.mpec_comp_positive = mpec.Complementarity(m.I, m.J, m.Y, m.L, rule = mpec_comp_positive)
m.mpec_comp_X        = mpec.Complementarity(m.I, m.J, m.Y,      rule = mpec_comp_X)

####  DATA              ########################################################

# --  Sets              ------------------------------------------------------ #

I = np.array(['F1', 'F2']) # Firms
J = np.array(['J1', 'J2']) # Technologies
Y = np.array(['Y1'])       # Years
L = np.array(['L1', 'L2']) # Load levels

# --  Indices           ------------------------------------------------------ #

T_idx      = pd.MultiIndex.from_product([    Y,L])
D_0_idx    = pd.MultiIndex.from_product([    Y,L])
alpha_idx  = pd.MultiIndex.from_product([    Y,L])
gamma_idx  = pd.MultiIndex.from_product([  J,Y  ])
X_idx      = pd.MultiIndex.from_product([I,J,Y  ])
grad_P_idx = pd.MultiIndex.from_product([    Y,L])

# --  Parameters        ------------------------------------------------------ #

T      = pd.Series([3760, 5000],     index =      T_idx) # [h]          Hours in a load level
D_0    = pd.Series([2000, 1400],     index =    D_0_idx) # [MW]         Demand_0
alpha  = pd.Series([   8,   12],     index =  alpha_idx) # [MW/(€/MWh)] Slope of the demand
gamma  = pd.Series([11.8,   22],     index =  gamma_idx) # [€/MWh]      Cost by product
X      = pd.Series([500]*len(X_idx), index =      X_idx) # [MW]         Technology capacity of a firm in one year
grad_P = pd.Series(-1/alpha.values,  index = grad_P_idx) # [-]          Price gradient

# --  Data Structure    ------------------------------------------------------ #
data = {None: {
    'I'      : {None: I},
    'J'      : {None: J},
    'Y'      : {None: Y},
    'L'      : {None: L},
    'T'      : T,
    'D_0'    : D_0,
    'alpha'  : alpha,
    'gamma'  : gamma,
    'X'      : X,
    'grad_P' : grad_P,
}}

####  SOLVE INSTANCE    ########################################################

m = m.create_instance(data)

solver = SolverFactory('path')
result = solver.solve(m, tee=True)

m.pprint()
m.display()
