import numpy as np
import pandas as pd
import pyomo.environ as pyo
from pyomo import mpec
from pyomo.opt import SolverFactory

# price = alpha -beta*quantity
players = np.array(['J1','J2'])
players_set = set(players)
alpha = 100
beta = 2
gamma = pd.Series(np.array([10,10]), index=players) # Cost of product of each player

def initialize(param):
    def initialize(*args):
        return param.__getitem__(*args[1:])
    return initialize

model          = pyo.ConcreteModel()
model.players  = pyo.Set(                 initialize = players)
model.alpha    = pyo.Param(               initialize = alpha)
model.beta     = pyo.Param(               initialize = beta)
model.gamma    = pyo.Param(model.players, initialize = initialize(gamma))
model.lagrange = pyo.Var(  model.players, domain = pyo.NonNegativeIntegers)
model.demand   = pyo.Var(  model.players, domain = pyo.NonNegativeIntegers) # qi

def gradient(model,i):
    """
        minimize - pi*qi + γi*qi ∀ i
            qi >= 0 -> -qi <= 0
            p = α-β(∑i q) -> p - (α-β(∑i q)) = 0

        lag = - pi*qi + γi*qi - λ*qi <= 0
        ∇pi = -β
        ∇lag = - (α - β(∑i qi)) - βqi) + γi - λ = 0
    """
    return (model.gamma[i] - (model.alpha - model.beta*model.demand[i] - model.beta*sum(model.demand[j] for j in model.players)) - model.lagrange[i]) == 0

def complementarity(model,i):
    return mpec.complements(model.lagrange[i] >= 0, model.demand[i] >= 0)

def dummy_obj(model):
    return 0

model.dummy_obj = pyo.Objective(rule=dummy_obj, sense = pyo.minimize)
model.gradient = pyo.Constraint(model.players, rule=gradient)
model.complementarity = mpec.Complementarity(model.players, rule=complementarity)

solver = SolverFactory('path')
result = solver.solve(model, tee=True)

model.pprint()
model.display()
