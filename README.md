# optimization-techniques

Completing the exercises of the optimization-techniques subject using pyomo.

Python 3.6.x (tested with python 3.6.8)

#### Dependencies
* pyomo
* pandas

## Installation steps

### Ubuntu 18.04 LTS (bionic beaver), 18.10 (cosmic cuttlefish), 19.04 (disco dingo), 19.10 (eoan ermine):
IMPORTANT: If you have troubles during the installation, see *Troubles* section below the installation steps.

```sh
sudo apt install python3.6
virtualenv venv --python=python3.6
source venv/bin/activate
pip install -r requirements.txt
```
#### solvers (ubuntu) (optionally)
* glpk (recommended for LP-MIP, free):
```sh
sudo apt install glpk-utils
```
* [path](http://pages.cs.wisc.edu/~ferris/path.html) (recommended for NCP, free):
```sh
sudo apt install libgfortran3
curl -O ftp://ftp.cs.wisc.edu/math-prog/solvers/path/ampl/lnx/pathampl_lnx.zip
unzip pathampl_lnx.zip
rm pathampl_lnx.zip
mv pathampl </path/to/common/binaries/added/in/$PATH/>
```

#### Troubles (The steps above doesn't work!)
IMPORTANT: If you have troubles during any *troubles* installation, please search it in *troubles* section too (recursivelly is better for you and me both).
About:
 * python3.6: If you are using Ubuntu 19.04 or 19.10 python3.6 is not in the repository anymore. Try to use python3.7 or install python3.6 using the source code, and don't forget to install SSL/TLS libraries before. Python3.6 installation from source code is not cover in this manual, maybe in the future is added to another folder and referenced here. Anyways, it is not hard googling.
 * libgfortran3: If you are using Ubuntu 19.10 libgfortran3 is not in the repository anymore. Follow the steps below under libgfortran tag (summarizing you have to download the deb package, install the dependencies and the package).

libgfortran3:
```sh
curl -OL http://cz.archive.ubuntu.com/ubuntu/pool/universe/g/gcc-6/gcc-6-base_6.5.0-2ubuntu1_amd64.deb
sudo apt install ./gcc-6-base_6.5.0-2ubuntu1_amd64.deb
curl -OL http://cz.archive.ubuntu.com/ubuntu/pool/universe/g/gcc-6/libgfortran3_6.5.0-2ubuntu1_amd64.deb
sudo apt install ./libgfortran3_6.5.0-2ubuntu1_amd64.deb
rm libgfortran3_6.5.0-2ubuntu1_amd64.deb
rm gcc-6-base_6.5.0-2ubuntu1_amd64.deb
```

### Windows 10 pro:

* Install [miniconda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/windows.html)
* Make virtualenv and install requirements.txt with pip install -r requirements.txt
* Install glpk or use another solver

## Usage

Every LP problem folder can be executed with python, i.e. if you want to execute the problem p1, type:
```sh
python LP-MIP/p1
```
or
```sh
python LP-MIP/p1/model.py
```

NCP problem execution can be performed with
```sh
python NCP/model.py
```
