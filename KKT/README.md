How to solve by KKT

* Pasar el problema modelado (f.obj y restricciones) a formato estandar.
* Calcular la derivada del lagrangiano: Calcular la derivada de la función + coeficientes lagrange e igualar a 0 (porque debe ser un mínimo/máximo)
* Ver las KKT para todos los puntos factibles


KKT conditions:
* Gradiente lagrangiano == 0
* Complementariedad: Lambda*(g(x)) == 0
* Lambdas >= 0
* Punto factible
