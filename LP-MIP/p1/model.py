import pandas as pd
import sys
import pathlib
import string
import pyomo.environ as pyo
from pyomo.opt import SolverFactory
if sys.argv[0].endswith(".py"):
    pardir = pathlib.Path(sys.argv[0]).parent
else:
    pardir = pathlib.Path(sys.argv[0])

def to_series(df):
    return pd.Series(df.reset_index().melt(id_vars=["index"]).set_index(["index", "variable"])["value"])
    

model = pyo.ConcreteModel(name = "Production of three products")

I = set(string.ascii_uppercase[:3])
J = set("F%d"%i for i in range(1,7))

capacity = pd.read_csv(pardir.joinpath("par/capacity.csv"), index_col = 0, header = None, squeeze = True)
production_df = pd.read_csv(pardir.joinpath("par/production.csv"), index_col = 0)
price = pd.read_csv(pardir.joinpath("par/price.csv"), index_col = 0, header = None, squeeze = True)
demand = pd.read_csv(pardir.joinpath("par/demand.csv"), index_col = 0, header = None, squeeze = True)

production = to_series(production_df)

model.I = pyo.Set(initialize=I)
model.J = pyo.Set(initialize=J)

model.C = pyo.Param(model.I, model.J, initialize=lambda m, i, j:production[i,j])
model.F = pyo.Param(model.J, initialize=lambda m, j:capacity[j])
model.P = pyo.Param(model.I, initialize=lambda m, i:price[i])
model.D = pyo.Param(model.I, initialize=lambda m, i:demand[i])

model.x = pyo.Var(model.I, model.J, domain = pyo.NonNegativeReals)

def objective(model):
    return sum(model.x[i,j]*(model.P[i]-model.C[i,j]) for i in model.I for j in model.J)

def constraint_1(model, j): # Capacity constraint
    return sum(model.x[i,j] for i in model.I) <= model.F[j]

def constraint_2(model, i): # Demand constraint
    return sum(model.x[i,j] for j in model.J) <= model.D[i]

model.Obj = pyo.Objective(rule=objective, sense = pyo.maximize)

model.constraint_capacity = pyo.Constraint(model.J, rule=constraint_1)
model.constraint_demand = pyo.Constraint(model.I, rule=constraint_2)

solver = SolverFactory('glpk')
result = solver.solve(model)

model.pprint()
model.display()
