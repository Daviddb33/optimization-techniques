import pandas as pd
import sys
import pathlib
import string
import pyomo.environ as pyo
import re
from pyomo.opt import SolverFactory

if sys.argv[0].endswith(".py"):
    pardir = pathlib.Path(sys.argv[0]).parent
elif __name__ != "__main__":
    pardir = pathlib.Path(__file__).parent
else:
    pardir = pathlib.Path(sys.argv[0])

def to_series(df, index_size = 1):
    df = df.reset_index()
    df = df.melt(id_vars=df.columns[:index_size])
    df = df.set_index(["index", "variable"])["value"]
    serie = pd.Series(df)
    return serie

def load_data(name, dims = 1):
    path = pardir.joinpath("par/%s.csv"%name)
    if dims >= 2:
        print("#"*20)
        print(name)
        with open(path) as f:
            index_size = len(re.search("^,*", f.readline()).group(0))
        if index_size > 1:
            index_range = list(range(index_size))
        print("Size:",index_size)
        print("Range:",index_range)
        df = pd.read_csv(path, index_col = index_range)
        print(df)
        data = to_series(df, index_size)
        print(data)
    elif dims == 1:
        data = pd.read_csv(path, index_col = 0, header = None, squeeze = True)
    elif dims == 0:
        data = pd.read_csv(path, header = None)[0][0]
    else:
        raise Exception("Bad dimension")
    return data

def make():
    model = pyo.ConcreteModel(name = "Production of three products")
    
    I = set(['XXX', 'YYY']) # Equipment
    J = set(['F1','F2']) # Assemblers
    K = set(['A', 'B', 'C']) # Components
    
    capacity = load_data("capacity", 3)
    demand = load_data("demand", 1)
    loss = load_data("loss", 1)
    profit = load_data("profit", 2)
    stock = load_data("stock", 1)
    
    model.I = pyo.Set(initialize=I)
    model.J = pyo.Set(initialize=J)
    model.K = pyo.Set(initialize=K)
    
    model.F = pyo.Param(model.I, initialize=lambda m, i:capacity[i])
    model.C = pyo.Param(model.I, model.J, initialize=lambda m, i, j:cost[i,j])
    model.CF = pyo.Param(initialize=fixed_cost)
    model.D = pyo.Param(initialize=demand)
    model.D = pyo.Param(initialize=demand)
    
    model.x = pyo.Var(model.I, model.J, domain = pyo.Binary)
    
    def objective(model):
        return sum(model.x[i,j]*(model.CF + model.C[i,j]) for i in model.I for j in model.J)  # NOTE: Multiply pyo*np works but backwards
    
    def constraint_1(model): # Demand constraint
        return sum(int(j)*model.F[i]*model.x[i,j] for i in model.I for j in model.J) >= model.D
    
    def constraint_2(model, i): # number selection constraint
        return sum(model.x[i,j] for j in model.J) <= 1
    
    model.Obj = pyo.Objective(rule=objective, sense = pyo.minimize)
    
    model.constraint_demand = pyo.Constraint(rule=constraint_1)
    model.constraint_selection = pyo.Constraint(model.I, rule=constraint_2)
    return model

def solve(model):
    solver = SolverFactory('glpk')
    result = solver.solve(model)
    
    model.pprint()
    model.display()

if __name__ == "__main__":
    model = make()
    solve(model)
