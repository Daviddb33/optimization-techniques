import pandas as pd
import sys
import pathlib
import string
import pyomo.environ as pyo
from pyomo.opt import SolverFactory
if sys.argv[0].endswith(".py"):
    pardir = pathlib.Path(sys.argv[0]).parent
else:
    pardir = pathlib.Path(sys.argv[0])

def to_series(df):
    return pd.Series(df.reset_index().melt(id_vars=["index"]).set_index(["index", "variable"])["value"])
    

model = pyo.ConcreteModel(name = "Production of three products")

I = set(string.ascii_uppercase[:3])
J = set("%d"%i for i in range(1,6))

capacity = pd.read_csv(pardir.joinpath("par/capacity.csv"), index_col = 0, header = None, squeeze = True)
cost_df = pd.read_csv(pardir.joinpath("par/cost.csv"), index_col = 0)
fixed_cost = float(pd.read_csv(pardir.joinpath("par/fixed_cost.csv"), header = None)[0][0])
demand = float(pd.read_csv(pardir.joinpath("par/demand.csv"), header = None)[0][0])

cost = to_series(cost_df)

model.I = pyo.Set(initialize=I)
model.J = pyo.Set(initialize=J)

model.F = pyo.Param(model.I, initialize=lambda m, i:capacity[i])
model.C = pyo.Param(model.I, model.J, initialize=lambda m, i, j:cost[i,j])
model.CF = pyo.Param(initialize=fixed_cost)
model.D = pyo.Param(initialize=demand)

model.x = pyo.Var(model.I, model.J, domain = pyo.Binary)

def objective(model):
    return sum(model.x[i,j]*(model.CF + model.C[i,j]) for i in model.I for j in model.J)  # NOTE: Multiply pyo*np works but backwards

def constraint_1(model): # Demand constraint
    return sum(int(j)*model.F[i]*model.x[i,j] for i in model.I for j in model.J) >= model.D

def constraint_2(model, i): # number selection constraint
    return sum(model.x[i,j] for j in model.J) <= 1

model.Obj = pyo.Objective(rule=objective, sense = pyo.minimize)

model.constraint_demand = pyo.Constraint(rule=constraint_1)
model.constraint_selection = pyo.Constraint(model.I, rule=constraint_2)

solver = SolverFactory('glpk')
result = solver.solve(model)

model.pprint()
model.display()
